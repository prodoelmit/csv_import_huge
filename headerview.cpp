#include "headerview.h"
#include <QTableWidget>
#include <QComboBox>
#include <QtAlgorithms>

HeaderView::HeaderView(QWidget *parent)
        : QHeaderView(Qt::Horizontal, parent)
        , m_negativeOffset(new int(0))
        , m_prevOffset(new int(0))
{


    connect(this, SIGNAL(sectionCountChanged(int,int)), this, SLOT(updateSectionsCount(int,int)));
    setMinimumHeight(20);

}

std::string HeaderView::getColumnName(int column)
{
    return m_comboboxes.at(column)->currentText().toStdString();
}

int HeaderView::getColumnNameID(int column)
{
    return m_comboboxes.at(column)->currentIndex();
}

void HeaderView::paintSection(QPainter *painter, const QRect &rect, int logicalIndex) const {
    if (*m_prevOffset != offset()) {
        *m_prevOffset = offset();
        for (int i = 0; i < model()->columnCount(); i++) {
            *(m_positioned[i]) = false;
        }
    }

    //        if (rect.x() < 0) {
    //            *m_negativeOffset = rect.x(); //yes, this is intended to be pointer, as HeaderView is const and we can't change it's members
    //            qDebug() << "changed m_negativeOffset to" << *m_negativeOffset;
    //        } else if (rect.x() == 0) {
    //            *m_negativeOffset = 0;
    //        }

    qDebug() << "paintSection()";
    painter->save();
    QHeaderView::paintSection(painter, rect, logicalIndex);
    painter->restore();
    //    QStyleOptionButton cb_option;
    //    cb_option.rect = QRect(rect.x() + 7,rect.y() + rect.height() / 2 - 7,14,14);
    //    if (m_isOn.at(logicalIndex)) {
    //        cb_option.state = QStyle::State_On;
    //    } else {
    //        cb_option.state = QStyle::State_Off;
    //    }
    //    this->style()->drawPrimitive(QStyle::PE_IndicatorCheckBox, &cb_option, painter);
//    if (!*(m_positioned.at(logicalIndex))) {
        {
            QCheckBox* checkbox = m_checkboxes.at(logicalIndex);
            int offset = 0;
            if (rect.x() < 0) {
                offset = rect.x();
            }
            QPoint p(rect.x() + 7 - *m_negativeOffset - offset, rect.y() + rect.height() / 2 - 7);
            QPoint pp = mapTo(parentWidget(), p) + QPoint(offset, 0);
            qDebug() << pp << logicalIndex;
            if (pp.x() >= 0) {
                checkbox->show();
                checkbox->move(pp);
                checkbox->resize(14,14);
            }
        }


        {
            QComboBox* combobox = m_comboboxes.at(logicalIndex);
            QPoint p(rect.x() + 28 - *m_negativeOffset, rect.y() + rect.height() * 0.1);
            QPoint pp = mapTo(parentWidget(), p);
            if (pp.x() >= 0) {
                combobox->show();
                combobox->move(pp);
                combobox->resize(rect.width() - 28 - 7, rect.height() * 0.8);
            }
        }
//        *(m_positioned[logicalIndex]) = true;
//    }

    *(m_visible.at(logicalIndex)) = true;

}

void HeaderView::mousePressEvent(QMouseEvent *event)
{
    //    QPoint pos = event->pos();
    //    int ind = logicalIndexAt(pos);
    //    m_checkboxes[ind]->setCheckState(
    //                m_checkboxes[ind]->checkState() == Qt::Checked ? Qt::Unchecked : Qt::Checked
    //                                                                 );
    //    update();
    //    QHeaderView::mousePressEvent(event);
}

QTableWidget *HeaderView::table()
{

    QTableWidget* table = dynamic_cast<QTableWidget*>(parentWidget());
    return table;

}

void HeaderView::paintEvent(QPaintEvent *e)
{
    for (int i = 0; i < model()->columnCount(); i++) {
                m_comboboxes.at(i)->move(5000, 0);
                m_checkboxes.at(i)->move(5000, 0);
                *(m_visible.at(i)) = false;
    }

    QHeaderView::paintEvent(e);

    for (int i = 0; i < model()->columnCount(); i++) {
        if (!*(m_visible.at(i))) {
            m_comboboxes.at(i)->hide();
            m_checkboxes.at(i)->hide();
        }
    }


}


void HeaderView::updateSectionsCount(int oldCount, int newCount)
{
    m_isOn.clear();
    for (int i = 0; i < newCount; i++) {
        m_isOn << true;
    }

    qDeleteAll(m_checkboxes);
    m_checkboxes.clear();
    int offset = 0;
    for (int i = 0; i < newCount; i++) {
        QCheckBox* cb = new QCheckBox(this->parentWidget());
        cb->setChecked(true);
        //        int rectLeft = sectionPosition(i);
        //        qDebug() << rectLeft;
        //        if (rectLeft < 0) {
        ////            offset = rectLeft;
        //        }
        //        QPoint p(rectLeft + 7 - *m_negativeOffset - offset, height() / 2 - 7);
        //        QPoint pp = mapTo(parentWidget(), p) + QPoint(offset, 0);
        //        cb->show();
        //        cb->move(pp);
        //        cb->resize(14,14);
        m_checkboxes << cb;
        connect(cb, SIGNAL(clicked(bool)), this, SLOT(toggleColumnEnabled(bool)));
    }
    qDeleteAll(m_comboboxes);
    m_comboboxes.clear();
    for (int i = 0; i < newCount; i++) {
        QComboBox* cb = new QComboBox(this->parentWidget());
        m_comboboxes << cb;
    }



    updateHeaders();

}

void HeaderView::toggleColumnEnabled(bool enabled)
{
    QCheckBox* cb = qobject_cast<QCheckBox*>(QObject::sender());

    int ind = m_checkboxes.indexOf(cb);
    for (int i = 0; i <  table()->rowCount(); i++) {
        QTableWidgetItem* item = table()->item(i, ind);
        if (enabled) {
            item->setFlags(item->flags() | Qt::ItemIsEnabled);
        } else {
            item->setFlags(item->flags() & (~Qt::ItemIsEnabled));
        }
    }
    m_comboboxes.at(ind)->setEnabled(enabled);

}

void HeaderView::updateHeaders()
{
    table()->resizeColumnsToContents();

    if (!model()) {
        return;
    }
    int newCount = model()->columnCount();
    if (newCount == 0) {
        return;
    }
    m_possibleHeaders.clear();
    m_headers.clear();
    qDeleteAll(m_positioned);
    m_positioned.clear();
    qDeleteAll(m_visible);
    m_visible.clear();




    QFontMetrics fm(table()->font());
    for (int i = 0; i < newCount; i++) {
        QString s = model()->headerData(i, Qt::Horizontal).toString();
        m_headers.append(s);
        QVector<QString> list;
        list << s << "h1" << "h2" << "h3" << "h4" << "h5";
        m_possibleHeaders << list;
        QComboBox* cb = m_comboboxes[i];
        int prevInd = cb->currentIndex();
        if (prevInd == -1) {
            prevInd = 0;
        }
        cb->clear();
        cb->insertItems(0, list.toList());
        cb->setCurrentIndex(qMin(prevInd, list.size() - 1) );
        setResizeMode(i, Fixed);
        int minWidth = fm.width(s) + 28 + 7 + 28;
        if (table()->columnWidth(i) < minWidth) {
            table()->setColumnWidth(i, minWidth);
        }
        m_positioned << new bool(false);
        m_visible << new bool(false);
    }

}
