#include "csvwindow.h"
#include "headerwidgetitem.h"
#include <QFileInfo>
#include <QUrl>
#include <QDir>
#include <QMimeData>
#include <cstdio>
#include "qdebug.h"
#include <QDropEvent>

using namespace std;

CSVWindow::CSVWindow(QWidget *parent)
    : QWidget(parent)
    , m_layout(new QGridLayout(this))
    , m_tWidget(new QTableWidget(this))
    , m_firstAsHeaderCB(new QCheckBox("Use first row as headers"))
    , m_firstAsHeader(false)
{
    //    loadCSVFile("/tmp/1.csv");
    //    qDebug() << m_data[10][19];
    connect(m_firstAsHeaderCB, SIGNAL(stateChanged(int)), this, SLOT(setFirstAsHeader(int)));
    m_layout->addWidget(m_tWidget);
    m_layout->addWidget(m_firstAsHeaderCB);
    m_hv = new HeaderView(m_tWidget);
    m_tWidget->setHorizontalHeader(m_hv);
    //    loadEverythingToTable();
    setAcceptDrops(true);
}


bool CSVWindow::loadCSVFile(string filename)
{
    clear();
    FILE* f = fopen(filename.c_str(), "r");
    if (f == NULL) {
        qDebug() << "Couldn't open file" << QString::fromStdString(filename);
        return false;
    }

    char line[1000];
    bool firstLine = true;
    while (!feof(f)) {
        char* gotit = fgets(line, 1000, f);
        if (gotit == NULL) {
            break;
        }
        QVector<QString> parsedLine = parseLine(line);
        if (parsedLine.isEmpty()) {
            continue;
        }
        if (firstLine){
            firstLine = false;
            m_headers = parsedLine;
            m_data << parsedLine; // doubling this line so that we always have headers as headers and they might optionally be used as first line of data
        } else {
            m_data << parsedLine;
        }
    }
    return true;
}

void CSVWindow::clear()
{
    m_data.clear();
}

void CSVWindow::loadEverythingToTable()
{
    int rowI = 0;
    int colI = 0;

    int rowCount = m_data.size();
    if (!rowCount) {
        return;
    }
    if (m_firstAsHeader) {
        m_tWidget->setRowCount(rowCount - 1);
    } else {
        m_tWidget->setRowCount(rowCount);
    }

    int colCount = m_data[0].size();
    m_tWidget->setColumnCount(m_data[0].size());
    int firstRow = m_firstAsHeader ? 1 : 0;
    for (int i = firstRow; i < rowCount; i++) {
        colI = 0;
        const QVector<QString>& row = m_data.at(i);
        for (int j = 0; j < colCount; j++) {
            const QString& s = row.at(j);
            QTableWidgetItem* item = new QTableWidgetItem(s);
            m_tWidget->setItem(rowI, colI, item);
            colI++;
        }
        rowI++;
    }

    QList<QString> headers;
    for (int i = 0; i < colCount; i++) {
        // prepare header item
        if (m_firstAsHeader) {
            headers << m_headers.at(i);
        } else {
            headers << QString::number(i+1);
        }
    }
    m_tWidget->setHorizontalHeaderLabels(headers);
    m_hv->updateHeaders();
}

void CSVWindow::dropEvent(QDropEvent *event)
{
    const QMimeData* m = event->mimeData();
    QUrl url = m->urls().first();

    QString path = url.toLocalFile();
    if (!QFile::exists(path)) {
        return;
    }
    event->accept();
    setStyleSheet("background-color: lightyellow");
    update();
    loadCSVFile(path.toStdString());
    loadEverythingToTable();
    setStyleSheet("");

}

void CSVWindow::dragEnterEvent(QDragEnterEvent *event)
{
    const QMimeData* m = event->mimeData();
    if (m->hasUrls()) {
        QString path = m->urls().first().toLocalFile().trimmed();
        if (!QFile::exists(path)) {
            return;
        }

        event->acceptProposedAction();
        setStyleSheet("background-color: lightblue");
    }
}

void CSVWindow::dragLeaveEvent(QDragLeaveEvent *event)
{
    setStyleSheet("");
}

int CSVWindow::getRowCount()
{
    return m_data.count();
}

int CSVWindow::getAllColumnCount()
{
    return m_data.first().count();
}

int CSVWindow::getEnabledColumnCount()
{
    return m_enabledColumns.count();
}

int CSVWindow::getColumnNameID(int column)
{
    return m_hv->getColumnNameID(column);
}

string CSVWindow::getColumnName(int column)
{
    return m_hv->getColumnName(column);
}

string CSVWindow::getValue(int row, int column)
{
    return m_data.at(row).at(column).toStdString();
}

bool CSVWindow::firstAsHeader() const
{
    return m_firstAsHeader;
}

void CSVWindow::addHeaderToData()
{
    QList<QString> headers;
    m_tWidget->insertRow(0);
    for (int i = 0; i < m_headers.size(); i++) {
        QTableWidgetItem* it = new QTableWidgetItem(m_headers.at(i));
        m_tWidget->setItem(0, i, it);
        headers << QString::number(i+1);
    }

    m_tWidget->setHorizontalHeaderLabels(headers);
}

void CSVWindow::useFirstRowAsHeader()
{
    if (m_tWidget->rowCount() == 0) {
        return;
    }

    m_tWidget->removeRow(0);
    for (int i = 0; i < m_headers.size(); i++) {
        m_tWidget->horizontalHeaderItem(i)->setText(m_headers.at(i));
    }
}

void CSVWindow::setFirstAsHeader(bool firstAsHeader)
{
    if (m_firstAsHeader == firstAsHeader)
        return;

    m_firstAsHeader = firstAsHeader;
    if (m_firstAsHeader) {
        useFirstRowAsHeader();
    } else {
        addHeaderToData();
    }
    m_hv->updateHeaders();
    emit firstAsHeaderChanged(firstAsHeader);
}

void CSVWindow::setFirstAsHeader(int checkState)
{
    bool fah = (checkState == (int)(Qt::Checked));
    setFirstAsHeader(fah);
}

QVector<QString> CSVWindow::parseLine(char* line)
{
    QVector<QString> words;
    char* p;
    char *newline = strchr(line, '\n');
    if (newline) {
        *newline = 0;
    }
    p = strtok(line, ",");
    while (p != NULL) {
        words << QString::fromLatin1(p);
        p = strtok(NULL, ",");
    }
    return words;
}
