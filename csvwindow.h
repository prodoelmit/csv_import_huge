#ifndef CSVWINDOW_H
#define CSVWINDOW_H
#include <QVector>
#include <QWidget>
#include <QString>
#include <string>
#include <QTableWidget>
#include <QGridLayout>
#include <QCheckBox>
#include <QLabel>
#include "headerview.h"


class CSVWindow: public QWidget
{
    Q_OBJECT
    Q_PROPERTY(bool firstAsHeader READ firstAsHeader WRITE setFirstAsHeader NOTIFY firstAsHeaderChanged)
public:
    CSVWindow(QWidget* parent = 0);

    bool loadCSVFile(std::string filename);
    void clear();
    void loadEverythingToTable();

    void dropEvent(QDropEvent* event);
    void dragEnterEvent(QDragEnterEvent* event);
    void dragLeaveEvent(QDragLeaveEvent *event);

    int getRowCount();
    int getAllColumnCount();
    int getEnabledColumnCount();
    int getColumnNameID(int column);
    std::string getColumnName(int column);
    std::string getValue(int row, int column);

    bool firstAsHeader() const;

    void addHeaderToData();
    void useFirstRowAsHeader();

public slots:
    void setFirstAsHeader(bool firstAsHeader);
    void setFirstAsHeader(int checkState);

signals:
    void firstAsHeaderChanged(bool firstAsHeader);

protected:
    QVector<QString> parseLine(char *line);
private:
    QVector<QString> m_headers;
    QVector<QVector<QString> > m_data;
    QCheckBox* m_firstAsHeaderCB;
    QSet<int> m_enabledColumns;

    QGridLayout* m_layout;
    QTableWidget* m_tWidget;
    bool m_firstAsHeader;

    HeaderView* m_hv;

};

#endif // CSVWINDOW_H
