#ifndef HEADERVIEW_H
#define HEADERVIEW_H
#include <QHeaderView>
#include <QPainter>
#include <QRect>
#include <qdebug.h>
#include <QMouseEvent>
#include <QCheckBox>
#include <QComboBox>

#include <QTableWidget>

class HeaderView: public QHeaderView
{
    Q_OBJECT
public:
    HeaderView(QWidget* parent = 0);


    std::string getColumnName(int column);
    int getColumnNameID(int column);
protected:
    void paintSection(QPainter *painter, const QRect &rect, int logicalIndex) const;

    void mousePressEvent(QMouseEvent* event);
    QTableWidget* table();
    void paintEvent(QPaintEvent *e);
public slots:

    void updateHeaders();

protected slots:
    void updateSectionsCount(int oldCount, int newCount);
    void toggleColumnEnabled(bool enabled);



private:
    QVector<bool> m_isOn;
    QVector<QString> m_headers;
    QVector<QVector<QString> > m_possibleHeaders;
    QVector<QCheckBox*> m_checkboxes;
    QVector<QComboBox*> m_comboboxes;
    int* m_negativeOffset;
    QVector<bool*> m_positioned;
    int* m_prevOffset;
    QVector<bool*> m_visible;

};


#endif // HEADERVIEW_H
