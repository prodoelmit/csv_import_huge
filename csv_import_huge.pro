#-------------------------------------------------
#
# Project created by QtCreator 2017-08-15T18:09:47
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = csv_import_huge
TEMPLATE = app


SOURCES += main.cpp\
        mw.cpp \
    csvwindow.cpp \
    headerwidgetitem.cpp \
    headerview.cpp

HEADERS  += mw.h \
    csvwindow.h \
    headerwidgetitem.h \
    headerview.h
