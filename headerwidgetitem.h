#ifndef HEADERWIDGETITEM_H
#define HEADERWIDGETITEM_H
#include <QTableWidgetItem>


class HeaderWidgetItem: public QObject, public QTableWidgetItem
{
    Q_OBJECT
public:
    HeaderWidgetItem(QWidget* parent = 0);

    static int userType();
};

#endif // HEADERWIDGETITEM_H
