#include "mw.h"
#include <QApplication>
#include "csvwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    CSVWindow* csvw = new CSVWindow;

    csvw->show();

    return a.exec();
}
