#include "headerwidgetitem.h"

HeaderWidgetItem::HeaderWidgetItem(QWidget *parent)
    : QObject(parent)
    , QTableWidgetItem(HeaderWidgetItem::userType())
{
    setFlags(Qt::ItemIsUserCheckable);
    setCheckState(Qt::Checked);
}

int HeaderWidgetItem::userType()
{
    return 1337;
}
